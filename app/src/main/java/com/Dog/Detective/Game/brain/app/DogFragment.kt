package com.Dog.Detective.Game.brain.app

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.Dog.Detective.Game.brain.app.databinding.FragmentDogBinding
import com.Dog.Detective.Game.brain.app.databinding.FragmentStartExitBinding
import eightbitlab.com.blurview.BlurView
import eightbitlab.com.blurview.RenderScriptBlur

class DogFragment : Fragment() {

    private var binding: FragmentDogBinding? = null
    private val mBinding get() = binding!!

    private var page = 1
    private lateinit var model : Dogdata
    private var currentAnswer = ""
    private var points = 0

    private lateinit var mBlurView: BlurView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDogBinding.inflate(layoutInflater, container, false)
        mBinding.apply {
            mBlurView = blur
        }
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.apply {
            initMainFields()
            next.isEnabled = false

            back.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
            first.setOnClickListener {
                logicButtons(first, firstText, next)
            }
            second.setOnClickListener {
                logicButtons(second, secondText, next)
            }
            third.setOnClickListener {
                logicButtons(third, thirdText, next)
            }
            next.setOnClickListener {
                next.isEnabled = false
                page++
                if (page == 10){
                   initResults()
                }
                else{
                    initMainFields()
                    enablingCards(true)
                }
            }
            toMenu.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initResults(){
        mBinding.apply {
            generationBlurEffect()
            back.isEnabled = false
            result.text = "$points/10"
        }
    }

    private fun logicButtons(card : CardView, text : TextView, next : CardView){
        next.isEnabled = true
        if(text.text == currentAnswer){
            card.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorGreen)
            points++
        }
        else{
            card.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorRed)
        }

        enablingCards(false)
    }

    private fun initMainFields(){
        model = DogCreate.gen(page)
        mBinding.apply {
            image.setImageResource(model.image)
            tittle.text = model.tittle
            firstText.text = model.first
            secondText.text = model.second
            thirdText.text = model.third
            currentAnswer = model.rightAnswer

            first.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.white)
            second.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.white)
            third.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.white)
        }
    }

    private fun enablingCards(boolean: Boolean){
        mBinding.apply {
            first.isEnabled = boolean
            second.isEnabled = boolean
            third.isEnabled = boolean
        }
    }

    private fun initFieldsForBlur(onResult:(radius : Float, rootView : ViewGroup?, windowBackground : Drawable?) -> Unit){
        val radius = 8f
        val decorView = activity?.window?.decorView
        val rootView = decorView?.findViewById<ViewGroup>(android.R.id.content)
        val windowBackground = decorView?.background
        onResult(radius, rootView, windowBackground)
    }

    private fun generationBlurEffect(){
        initFieldsForBlur { radius, rootView, windowBackground ->
            if (rootView != null) {
                mBlurView.setupWith(rootView, RenderScriptBlur(requireContext()))
                    .setFrameClearDrawable(windowBackground)
                    .setBlurRadius(radius)
            }
            mBlurView.visibility = View.VISIBLE
        }
    }

    private fun numberPlace() = 0

    private fun textGen() = "generation"

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}