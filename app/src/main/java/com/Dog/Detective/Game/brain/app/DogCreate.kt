package com.Dog.Detective.Game.brain.app


object DogCreate {
    fun gen(page : Int) : Dogdata{
        return when(page){
            1 -> Dogdata(R.drawable.akita, "Guess this dog breed?", "Husky", "Akita Inu", "Laika", "Akita Inu")
            2 -> Dogdata(R.drawable.pitbull, "What is this formidable breed of dog in front of you?", "Boxer", "American pit bull", "American Great Dane", "American pit bull")
            3 -> Dogdata(R.drawable.husky, "What kind of Husky is in the picture?", "Siberian Husky", "Husky Samoyed", "Japanese Husky", "Siberian Husky")
            4 -> Dogdata(R.drawable.chin, "What cute breed is shown in the photo?", "Shih Tzu", "Pekingese", "Japanese Chin", "Japanese Chin")
            5 -> Dogdata(R.drawable.hound, "Who is this handsome guy in the photo?", "Italian Greyhound", "Basset Hound", "Dachshund", "Basset Hound")
            6 -> Dogdata(R.drawable.bulldog, "Who is this cutie?", "American Bulldog", "French Bulldog", "Boxer", "French Bulldog")
            7 -> Dogdata(R.drawable.breed, "What breed?", "Zenenhund", "German pointer", "If", "If")
            8 -> Dogdata(R.drawable.shephard, "What breed?", "Asian Shepherd", "German Shepherd", "Caucasian Oacharka", "German Shepherd")
            9 -> Dogdata(R.drawable.dachshund, "This short-legged baby has won many hearts", "Beagle", "Jagdterrier", "Dachshund", "Dachshund")
            else -> Dogdata(R.drawable.newfoundland, "What breed of dog?", "Newfoundland", "Retriever", "Labrador", "Newfoundland")
        }
    }
}