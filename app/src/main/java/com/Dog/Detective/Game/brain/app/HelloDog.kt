package com.Dog.Detective.Game.brain.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper

class HelloDog : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello_dog)

        Handler(Looper.getMainLooper()).postDelayed({
            this.replaceActivity(Pattern())
        }, 500)
    }
}